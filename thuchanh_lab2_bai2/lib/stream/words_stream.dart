import 'dart:math';

import 'package:flutter/material.dart';

class WordsStream {

  Stream<String> getWords() async* {
    final List<String> words = [
      'Chicken',
      'Witcher',
      'Computer',
      'Football',
      'Internet',
    ];

    yield* Stream.periodic(Duration(seconds: 2), (int t) {
      int index = Random().nextInt(5);
      return words[index];
    });
  }
}