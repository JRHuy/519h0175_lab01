import 'package:flutter/material.dart';

class Product {
  final String image, title, description;
  final int? price, size, id;
  // final Color? color;

  Product({
    this.id,
    required this.image,
    required this.title,
    this.price,
    required this.description,
    this.size,
    // this.color,
  });
}

List<Product> products = [
  Product(
    id: 1,
    title: "Samsung Galaxy S22",
    price: 1300,
    size: 11,
    description: "Top android phone",
    image: "",
    // color: ,
  ),
  Product(
    id: 1,
    title: "iPhone 13",
    price: 1400,
    size: 11,
    description: "Good phone",
    image: "",
    // color: ,
  ),
  Product(
    id: 1,
    title: "Samsung Galaxy Note",
    price: 1300,
    size: 11,
    description: "Great phone",
    image: "",
    // color: ,
  ),
  Product(
    id: 1,
    title: "iPhone 14 Pro Max",
    price: 1300,
    size: 11,
    description: "Good phone",
    image: "",
    // color: ,
  ),
  Product(
    id: 1,
    title: "Samsung Galaxy Fold",
    price: 1300,
    size: 11,
    description: "Good phone",
    image: "",
    // color: ,
  ),
];