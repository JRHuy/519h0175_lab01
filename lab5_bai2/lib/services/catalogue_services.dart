// Use relative path: NOK
// import '../repositories/repository.dart';
import '../repositories/repository.dart';
import '../models/product.dart';
import '../models/catalogue.dart';
// Use relative path: NOK
import '../repositories/in_memory_cache.dart';

class CatalogueServices {
  Repository _repository = InMemoryCache();

  Catalogue createPlan(String name) {
    final model = _repository.create();
    final catalogue = Catalogue.fromModel(model)..name = name;
    saveCatalogue(catalogue);
    return catalogue;
  }

  void saveCatalogue(Catalogue catalogue) {
    _repository.update(catalogue.toModel());
  }

  void delete(Catalogue catalogue) {
    _repository.delete(catalogue.toModel().id);
  }

  List<Catalogue> getAllCatalogues() {
    return _repository
        .getAll()
        .map<Catalogue>((model) => Catalogue.fromModel(model))
        .toList();
  }

  void addTask(Catalogue catalogue, String name) {
    print('plan.tasks=${catalogue.products}');
    print('plan.tasks.last=${catalogue.products.length}');
    // final id = plan.tasks.last?.id ?? 0 + 1;
    final id = (catalogue.products.length > 0) ? catalogue.products.last.id : 1;
    final product = Product(id: id, name: name);
    catalogue.products.add(product);
    saveCatalogue(catalogue);
  }

  void deleteTask(Catalogue catalogue, Product product) {
    catalogue.products.remove(product);
    saveCatalogue(catalogue);
  }
}