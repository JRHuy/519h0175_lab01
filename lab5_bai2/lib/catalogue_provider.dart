import 'package:flutter/material.dart';
import 'controllers/catalogue_controller.dart';

class CatalogueProvider extends InheritedWidget {
  final _controller = CatalogueController();

  CatalogueProvider({Key? key, required Widget child}) : super(key: key, child: child);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => false;

  static CatalogueController of(BuildContext context) {
    CatalogueProvider provider = context.dependOnInheritedWidgetOfExactType<CatalogueProvider>() as CatalogueProvider;
    return provider._controller;
  }
}