import '../repositories/repository.dart';
import 'package:flutter/foundation.dart';
import 'product.dart';

class Catalogue {
  int id = 0;
  String name = '';
  List<Product> products = [];

  Catalogue({required this.id, this.name = ''});

  Catalogue.fromModel(Model model) {
    id = model.id;
    name = model.data['name'] ?? '';
    if (model.data['product'] != null) {
      products = model.data['product'].map<Product>((product) => Product.fromModel(product)).toList();
    }
  }


  // Plan.fromModel(Model model)
  //     : id = model.id,
  //       name = model.data['name'] ?? '',
  //       tasks = model.data['task']
  //           .map<Task>((task) => Task.fromModel(task)).toList() ??
  //           <Task>[];

  Model toModel() => Model(id: id, data: {
    'name': name,
    'tasks': products.map((task) => task.toModel()).toList()
  });

  // int get completeCount => products.where((task) => task.complete).length;

  String get countMessage =>
      '${products.length} products';
}
