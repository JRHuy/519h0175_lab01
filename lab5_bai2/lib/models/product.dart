import '../repositories/repository.dart';
import 'package:flutter/foundation.dart';

class Product {
  final int id;
  String name;

  Product({
    required this.id,
    this.name = '',
  });

  // model: properties id
  // model.data contains keys: description, complete
  Product.fromModel(Model model)
      : id = model.id,
        name = model.data['name'];

  Model toModel() =>
      Model(id: id, data: {'name': name});
}
