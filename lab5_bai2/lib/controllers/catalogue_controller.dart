import '../services/catalogue_services.dart';
import '../models/data_layer.dart';

class CatalogueController {
  final services = CatalogueServices();

  // This public getter cannot be modified by any other object
  List<Catalogue> get catalogues => List.unmodifiable(services.getAllCatalogues());

  void addNewCatalogue(String name) {
    if (name.isEmpty) {
      return;
    }
    name = _checkForDuplicates(catalogues.map((catalogue) => catalogue.name), name);
    services.createPlan(name);
  }

  void saveCatalogue(Catalogue catalogue) {
    services.saveCatalogue(catalogue);
  }

  void deletePlan(Catalogue catalogue) {
    services.delete(catalogue);
  }

  void createNewTask(Catalogue catalogue, [String? name]) {
    if (name == null || name.isEmpty) {
      name = 'New Task';
    }

    name = _checkForDuplicates(
        catalogue.products.map((product) => product.name), name);

    services.addTask(catalogue, name);
  }

  void deleteTask(Catalogue catalogue, Product product) {
    services.deleteTask(catalogue, product);
  }

  String _checkForDuplicates(Iterable<String> items, String text) {
    final duplicatedCount = items.where((item) => item.contains(text)).length;
    if (duplicatedCount > 0) {
      text += ' ${duplicatedCount + 1}';
    }

    return text;
  }
}