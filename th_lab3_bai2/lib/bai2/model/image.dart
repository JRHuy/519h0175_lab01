class ImageModel {
  final String url;
  final String title;

  const ImageModel({
    required this.url,
    required this.title,
  });

  factory ImageModel.fromJson(Map<String, dynamic> json) {
    return ImageModel(
      url: json['url'],
      title: json['title'],
    );
  }
}
