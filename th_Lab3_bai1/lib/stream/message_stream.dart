import 'package:flutter/material.dart';

class MessageStream {
  Stream<String> getMessage() async* {
    final List<String> messages = [
      "This is the life!",
      "Harry Potter was created by J.K.Rowling.",
      "Steve Jobs co-founded Apple in April 1, 1976.",
      "Welcome to Flutter Class!",
      "Samsung is the biggest Android brand",
      "We will have a class at 1PM"
    ];

    yield* Stream.periodic(Duration(seconds: 5), (int t) {
      int index = t % 6;
      return messages[index];
    });
  }
}