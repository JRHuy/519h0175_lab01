import 'package:flutter/material.dart';
import '../stream/message_stream.dart';

// class App extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return HomePageStream();
//   }
//
// }

class App extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }
}

class HomePageState extends State<StatefulWidget> {
  MessageStream messageStream = MessageStream();
  List<String> message = [];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Stream Color',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Bài 1'),
        ),
        // backgroundColor: bgTextColor,
        body: ListView.separated(
            itemBuilder: (BuildContext context, int index) {
              return Container(
                child: Center(child: Text('${message[index]}'),),
              );
            },
            separatorBuilder: (BuildContext context, int index) => const Divider(),
            itemCount: message.length),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.play_arrow_rounded),
          onPressed: () {
            sendMessage();
          },
        ),
      ),
    );
  }

  sendMessage() async {
    messageStream.getMessage().listen((messageEvent) {
      setState(() {
        print(messageEvent);
        message.add(messageEvent);
      });
    });
  }
}
