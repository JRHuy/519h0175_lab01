import 'package:flutter/material.dart';
import '../validation/mixins_validation.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Login App',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Login'),
        ),
        body: LoginScreen(),
      ),
    );
  }
}

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginState();
  }
}

class LoginState extends State<StatefulWidget> with CommonValidation{
  final formKey = GlobalKey<FormState>();
  late String emailAddress;
  late String surname;
  late String name;
  late String year;
  late String address;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Form(
        key: formKey,
        child: Column(
          children: [
            fieldEmail(),
            Container(margin: EdgeInsets.only(top: 20.0),),
            fieldSurname(),
            Container(margin: EdgeInsets.only(top: 20.0),),
            fieldName(),
            Container(margin: EdgeInsets.only(top: 20.0),),
            fieldBirthYear(),
            Container(margin: EdgeInsets.only(top: 20.0),),
            fieldAddress(),
            Container(margin: EdgeInsets.only(top: 20.0),),
            loginButton(),
          ],
        ),
      )
    );
  }

  Widget fieldEmail() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
          icon: Icon(Icons.email),
          labelText: 'Email Address'),
      validator: validateEmail,
      onSaved: (value) {
        emailAddress = value as String;
      },
    );
  }

  Widget fieldSurname() {
    return TextFormField(
      keyboardType: TextInputType.name,
      decoration: InputDecoration(
          icon: Icon(Icons.person_outline),
          labelText: 'Last and middle name'
      ),
      validator: validateBlank,
      onSaved: (value) {
        surname = value as String;
      }
    );
  }

  Widget fieldName() {
    return TextFormField(
      keyboardType: TextInputType.name,
      decoration: InputDecoration(icon: Icon(Icons.person), labelText: 'Name'),
      validator: validateBlank,
      onSaved: (value) {
        name = value as String;
      },
    );
  }

  Widget fieldBirthYear() {
    return TextFormField(
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        icon: Icon(Icons.calendar_today),
        labelText: 'Year',
      ),
      validator: validateYear,
      onSaved: (value) {
        year = value as String;
      },
    );
  }

  Widget fieldAddress() {
    return TextFormField(
      keyboardType: TextInputType.streetAddress,
      decoration: InputDecoration(
        icon: Icon(Icons.home),
        labelText: 'Address',
      ),
      validator: validateBlank,
      onSaved: (value) {
        address = value as String;
      },
    );
  }

  Widget loginButton() {
    return ElevatedButton(
        onPressed: () {
          if (formKey.currentState!.validate()) {
            // Call API Authentication from Backend
            formKey.currentState!.save();
            print('$emailAddress, $year, $surname, $name, $address');
          }
        },
        child: Text('Login')
    );
  }
}
