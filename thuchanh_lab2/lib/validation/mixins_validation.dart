mixin CommonValidation {
  String? validateBlank(String? value) {
    if (value == '') {
      return 'Fill out all fields';
    }
  }

  String? validateEmail(String? value) {
    if (!value!.contains('@')) {
      return 'Please input valid email.';
    }
    return null;
  }

  String? validateYear(String? value) {
    if (value! == DateTime.now().year.toString()) {
      return 'You cannot be born in this year';
    }

    return null;
  }
}