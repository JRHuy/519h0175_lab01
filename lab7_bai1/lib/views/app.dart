import 'package:flutter/material.dart';
import 'homescreen.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'MyApp',
      home: HomeScreen(),
    );
  }

}