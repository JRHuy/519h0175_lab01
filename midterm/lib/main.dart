import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:midterm/app.dart';
import 'package:midterm/views/register.dart';
import '/views/welcome.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Colors.lightBlue
      ),
      debugShowCheckedModeBanner: false,
      home: Welcome(),

      routes: <String,WidgetBuilder>{

        "Login" : (BuildContext context)=>Welcome(),
        "Register" : (BuildContext context)=>Register(),
        "Home" : (BuildContext context)=>MyTiki(),
      },

      onUnknownRoute: (settings) {
        return MaterialPageRoute<void>(
          settings: settings,
          builder: (BuildContext context) {
            Navigator.pop(context);
            return const Scaffold(body: Center(child: Text('Not Found')));
          },
        );
      },
    );
  }

}