import 'package:community_material_icon/community_material_icon.dart';
import 'package:flutter/material.dart';
import 'bottomnavigation.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int _selectedIndex = 0;
  Widget _home = Home();
  Widget _categories = Categories();
  Widget _products = Products();
  // Widget _Chat = Chat();
  Widget _personal = Personal();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: this.getBody(),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: this._selectedIndex,
        selectedItemColor: Colors.blue[800],
        unselectedItemColor: Colors.grey,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Trang chủ',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.category),
            label: 'Danh mục',
          ),
          BottomNavigationBarItem(
            icon: Icon(CommunityMaterialIcons.cart_variant),
            label: 'Sản phẩm',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: 'Cá nhân',
          )
        ],
        onTap: (int index) {
          this.onTapHandler(index);
        },
      ),
    );
  }

  Widget getBody() {
    if(this._selectedIndex == 0) {
      return this._home;
    } else if (this._selectedIndex == 1) {
      return this._categories;
    } else if (this._selectedIndex == 2) {
      return this._products;
    } else {
      return this._personal;
    }
  }

  void onTapHandler(int index) {
    setState(() {
      this._selectedIndex = index;
    });
  }
}
