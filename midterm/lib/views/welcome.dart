import 'dart:ffi';
import 'dart:ui';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:midterm/app.dart';
import 'register.dart';
import 'package:flutter/material.dart';

class Welcome extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _WelcomeState();
  }
}

class _WelcomeState extends State<Welcome> {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  late User user;
  bool isLoggedin = false;

  Future<UserCredential> googleSignIn() async {
    GoogleSignIn googleSignIn = GoogleSignIn();
    GoogleSignInAccount? googleUser = await googleSignIn.signIn();
    if (googleUser != null) {
      GoogleSignInAuthentication googleAuth = await googleUser.authentication;

      if (googleAuth.idToken != null && googleAuth.accessToken != null) {
        final AuthCredential credential = GoogleAuthProvider.credential(
            accessToken: googleAuth.accessToken, idToken: googleAuth.idToken);

        final UserCredential user =
        await _auth.signInWithCredential(credential);

        await Navigator.push(context, MaterialPageRoute(
            builder: (context) =>
                MyTiki())
        );

        return user;
      } else {
        throw StateError('Missing Google Auth Token');
      }
    } else {
      throw StateError('Sign in Aborted');
    }
  }

  navigateToRegister() async {
    Navigator.push(context, MaterialPageRoute(builder: (context) => Register()));
  }

  checkAuthentification() async {
    _auth.authStateChanges().listen((user) {
      if (user != null) {
        print(user);

        Navigator.pushReplacementNamed(context, "Home");
      }
    });
  }

  @override
  void initState() {
    super.initState();
    // this.checkAuthentification();
  }


  late String _email, _password;

  login() async {
    if(_formKey.currentState!.validate()) {
      _formKey.currentState!.save();
      try {
        UserCredential user = await _auth.signInWithEmailAndPassword(email: _email, password: _password);
        Navigator.pushReplacementNamed(context, "Home");
      }
      catch(e) {
        print(e);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 25.0,
          ),
          Container(
            height: 200,
            child: Image(
                image: AssetImage("images/tiki_logo.png"),
                height: 50,
                width: 350,
                fit: BoxFit.contain),
          ),
          SizedBox(
            height: 20,
          ),
          RichText(
            text: TextSpan(
              text: 'Chào mừng đến My Tiki',
              style: TextStyle(
                  fontSize: 25.0,
                  fontWeight: FontWeight.bold,
                  color: Colors.lightBlue),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            'Đăng nhập hoặc tạo tài khoản',
            style: TextStyle(color: Colors.grey),
          ),
          Container(
            child: Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  Container(
                    child: TextFormField(
                        validator: (input) {
                          if (input!.isEmpty) {
                            return 'Enter email';
                          }
                        },
                        decoration: InputDecoration(
                            labelText: 'Email', prefixIcon: Icon(Icons.email)),
                        onSaved: (input) {
                          if(input != null) {
                            _email = input;
                          }
                        }),
                  ),
                  Container(
                    child: TextFormField(
                        validator: (input) {
                          if (input!.length <= 8) {
                            return 'Password needs to have at least 8 characters';
                          }
                        },
                        decoration: InputDecoration(
                            labelText: 'Password',
                            prefixIcon: Icon(Icons.password)),
                        obscureText: true,
                        onSaved: (input) {
                          if(input != null) {
                            _password = input;
                          }
                        }),
                  ),
                ],
              ),
            ),
          ),

          SizedBox(height: 10,),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              RaisedButton(
                onPressed: login,
                child: Text(
                  'LOGIN',
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                color: Colors.lightBlue,
              ),

              // SizedBox(width: 20,),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('Chưa có tài khoản?', style: TextStyle(color: Colors.grey),),
              SizedBox(width: 10,),
              RaisedButton(
                onPressed: navigateToRegister,
                child: Text(
                  'REGISTER',
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                color: Colors.lightBlue,
              ),
            ],
          ),
          SignInButton(
            Buttons.Google,
            text: "Sign up with Google",
            onPressed: googleSignIn,
          ),
        ],
      ),
    ));
  }
}
