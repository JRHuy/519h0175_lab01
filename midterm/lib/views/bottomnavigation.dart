import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:midterm/views/welcome.dart';
import 'package:midterm/widget/banner_carousel.dart';
import 'package:midterm/widget/box.dart';
import 'package:midterm/widget/boxDetails.dart';
import '../mock_data.dart' as mockData;
import 'package:community_material_icon/community_material_icon.dart';

buildCategories() {
  return mockData.categories.map((category) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.all(16.0),
      child: Column(
        // crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
              height: 72.0,
              child: AspectRatio(
                aspectRatio: 1.0,
                child: Image(
                  image: category.image,
                  fit: BoxFit.cover,
                ),
              )),
          Expanded(
              child: Container(
            padding: EdgeInsets.only(top: 8.0),
            child: Text(
              category.name,
              maxLines: 2,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 14.0),
            ),
          )),
        ],
      ),
    );
  }).toList();
}

buildProducts() {
  return mockData.products.map((product) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.all(16.0),
      child: Column(
        // crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
              height: 72.0,
              child: AspectRatio(
                aspectRatio: 1.0,
                child: Image(
                  image: product.image,
                  fit: BoxFit.cover,
                ),
              )),
          Expanded(
              child: Container(
            padding: const EdgeInsets.all(16.0),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    product.name,
                    maxLines: 2,
                    style:
                        TextStyle(fontSize: 13.0, fontWeight: FontWeight.w700),
                    overflow: TextOverflow.ellipsis,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8.0),
                    child: Text(
                      product.salePrice,
                      style: TextStyle(
                          fontSize: 17.0,
                          color: Colors.red,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
            ),
          )),
        ],
      ),
    );
  }).toList();
}

buildHotProduct(int a) {
  return Container(
    height: 100.0,
    child: Column(
      children: <Widget>[
        Container(
          height: 100.0,
          child: AspectRatio(
            aspectRatio: 1.0,
            child: Image(
              image: mockData.products[a].image,
              fit: BoxFit.cover,
            ),
          ),
        ),
        Expanded(
            child: Container(
          padding: const EdgeInsets.all(16.0),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  mockData.products[a].name,
                  maxLines: 2,
                  style: TextStyle(fontSize: 13.0, fontWeight: FontWeight.w700),
                  overflow: TextOverflow.ellipsis,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  child: Text(
                    mockData.products[a].salePrice,
                    style: TextStyle(
                        fontSize: 17.0,
                        color: Colors.red,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ],
            ),
          ),
        )),
      ],
    ),
  );
}

class Categories extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: <Widget>[
        SliverAppBar(
          title: Text("TIKI.VN"),
          actions: <Widget>[
            Stack(
              children: <Widget>[
                IconButton(
                  icon: Icon(Icons.shopping_cart),
                  onPressed: () {},
                ),
                Positioned(
                    top: 4.0,
                    right: 8.0,
                    child: Container(
                      width: 16.0,
                      height: 16.0,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.deepOrangeAccent,
                      ),
                      child: Text(
                        "3",
                        style: TextStyle(
                            fontSize: 12.0, fontWeight: FontWeight.bold),
                      ),
                    ))
              ],
            )
          ],
          expandedHeight: 120.0,
          pinned: true,
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(20.0),
            child: Container(
              padding: EdgeInsets.all(16.0),
              child: Container(
                height: 44.0,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(5.0),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.black.withOpacity(0.2),
                          blurRadius: 5.0,
                          offset: Offset(0.0, 0.0)),
                    ]),
                child: Row(
                  children: <Widget>[
                    IconButton(
                        onPressed: () {},
                        icon: Icon(
                          Icons.search,
                          color: Colors.grey[600],
                        )),
                    Expanded(
                      child: Text(
                        "Bạn tìm gì hôm nay?",
                        style:
                            TextStyle(color: Colors.grey[600], fontSize: 16.0),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
        SliverList(
            delegate: SliverChildListDelegate([
          BoxDetails(
            title: "Danh mục các sản phẩm",
            child: SizedBox(
              height: 500.0,
              child: GridView.count(
                crossAxisCount: 2,
                children: buildCategories(),
                childAspectRatio: 1.0,
              ),
            ),
          )
        ])),
      ],
    );
  }
}

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: <Widget>[
        SliverAppBar(
          title: Text("TIKI.VN"),
          actions: <Widget>[
            Stack(
              children: <Widget>[
                IconButton(
                  icon: Icon(Icons.shopping_cart),
                  onPressed: () {},
                ),
                Positioned(
                    top: 4.0,
                    right: 8.0,
                    child: Container(
                      width: 16.0,
                      height: 16.0,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.deepOrangeAccent,
                      ),
                      child: Text(
                        "3",
                        style: TextStyle(
                            fontSize: 12.0, fontWeight: FontWeight.bold),
                      ),
                    ))
              ],
            )
          ],
          expandedHeight: 120.0,
          pinned: true,
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(20.0),
            child: Container(
              padding: EdgeInsets.all(16.0),
              child: Container(
                height: 44.0,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(5.0),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.black.withOpacity(0.2),
                          blurRadius: 5.0,
                          offset: Offset(0.0, 0.0)),
                    ]),
                child: Row(
                  children: <Widget>[
                    IconButton(
                        onPressed: () {},
                        icon: Icon(
                          Icons.search,
                          color: Colors.grey[600],
                        )),
                    Expanded(
                      child: Text(
                        "Bạn tìm gì hôm nay?",
                        style:
                            TextStyle(color: Colors.grey[600], fontSize: 16.0),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
        BannerCarousel(
          items: mockData.home_banners,
          aspectRatio: 1.6,
        ),
        SliverList(
            delegate: SliverChildListDelegate([
          Box(
            // title: Text(
            //   "Danh Mục Nổi Bật",
            //   style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
            // ),
            title: "Danh mục nổi bật",
            action: TextButton(
              onPressed: () {},
              child: Text("Xem tất cả"),
            ),
            child: Container(
              height: 288.0,
              child: GridView.count(
                crossAxisCount: 2,
                children: buildCategories(),
                scrollDirection: Axis.horizontal,
                childAspectRatio: 1.0,
              ),
            ),
          )
        ])),
        SliverToBoxAdapter(
          child: Container(
            color: Colors.white,
            margin: EdgeInsets.symmetric(vertical: 8.0),
            child: ListTile(
              leading: Icon(
                CommunityMaterialIcons.ticket_percent,
                size: 40.0,
                color: Colors.amberAccent,
              ),
              title: Text("Ưu đãi & hợp tác"),
              subtitle: Text(
                "Các ưu đãi cho chủ thẻ ngân hàng",
                style: TextStyle(fontSize: 12.0),
              ),
              trailing: IconButton(
                icon: Icon(
                  Icons.chevron_right,
                  size: 32.0,
                  color: Colors.blue,
                ),
                onPressed: () {},
              ),
            ),
          ),
        ),
        SliverToBoxAdapter(
            child: Box(
          title: "Đánh giá nhà cung cấp",
          action: TextButton(
            onPressed: () {},
            child: Text("Xem tất cả"),
          ),
          child: Container(
              padding: EdgeInsets.all(16.0),
              child: Row(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 8.0),
                    height: 100,
                    width: 100,
                    child: AspectRatio(
                      aspectRatio: 1.0,
                      child: Image(image: mockData.products[2].image),
                    ),
                  ),
                  Expanded(
                      child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        mockData.products[2].name,
                        style: TextStyle(
                          fontSize: 16.0,
                        ),
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8.0),
                        child: Text("Cung cấp bởi Praza"),
                      ),
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(primary: Colors.red),
                        onPressed: () {},
                        child: Text("VIẾT NHẬN XÉT"),
                      )
                    ],
                  ))
                ],
              )),
        )),
        SliverToBoxAdapter(
          child: Container(
            margin: EdgeInsets.symmetric(vertical: 8.0),
            height: 690.0,
            padding: EdgeInsets.all(18.0),
            decoration: BoxDecoration(
                gradient: LinearGradient(
              colors: [
                new Color.fromRGBO(247, 55, 127, 1.0),
                new Color.fromRGBO(250, 118, 97, 1.0),
              ],
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
            )),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(bottom: 16.0),
                  child: Text(
                    "Tiki Hot",
                    style: Theme.of(context)
                        .textTheme
                        .titleMedium
                        ?.copyWith(color: Colors.white),
                  ),
                ),
                Expanded(
                    child: Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(4.0)),
                  child: GridView.count(
                    crossAxisCount: 2,
                    children: <Widget>[
                      buildHotProduct(8),
                      buildHotProduct(10),
                      buildHotProduct(9),
                      buildHotProduct(12),
                      buildHotProduct(0),
                      buildHotProduct(14),
                    ],
                  ),
                )),
              ],
            ),
          ),
        )
      ],
    );
  }
}

class Products extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: <Widget>[
        SliverAppBar(
          title: Text("TIKI.VN"),
          actions: <Widget>[
            Stack(
              children: <Widget>[
                IconButton(
                  icon: Icon(Icons.shopping_cart),
                  onPressed: () {},
                ),
                Positioned(
                    top: 4.0,
                    right: 8.0,
                    child: Container(
                      width: 16.0,
                      height: 16.0,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.deepOrangeAccent,
                      ),
                      child: Text(
                        "3",
                        style: TextStyle(
                            fontSize: 12.0, fontWeight: FontWeight.bold),
                      ),
                    ))
              ],
            )
          ],
          expandedHeight: 120.0,
          pinned: true,
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(20.0),
            child: Container(
              padding: EdgeInsets.all(16.0),
              child: Container(
                height: 44.0,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(5.0),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.black.withOpacity(0.2),
                          blurRadius: 5.0,
                          offset: Offset(0.0, 0.0)),
                    ]),
                child: Row(
                  children: <Widget>[
                    IconButton(
                        onPressed: () {},
                        icon: Icon(
                          Icons.search,
                          color: Colors.grey[600],
                        )),
                    Expanded(
                      child: Text(
                        "Bạn tìm gì hôm nay?",
                        style:
                            TextStyle(color: Colors.grey[600], fontSize: 16.0),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
        SliverList(
            delegate: SliverChildListDelegate([
          BoxDetails(
            title: "Các sản phẩm",
            child: SizedBox(
              height: 450.0,
              child: GridView.count(
                crossAxisCount: 2,
                children: buildProducts(),
                childAspectRatio: 1.0,
              ),
            ),
          )
        ])),
      ],
    );
  }
}

class Personal extends StatefulWidget {
  @override
  State<Personal> createState() => _PersonalState();
}

class _PersonalState extends State<Personal> {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  User? user;
  bool isLoggedIn = false;

  checkAuthentification() async {
    _auth.authStateChanges().listen((user) {
      if (user == null) {
        // print(user);

        Navigator.push(context, MaterialPageRoute(
            builder: (context) =>
            Welcome())
        );
      }
    });
  }

  getUser() async {
    User firebaseUser = _auth.currentUser!;
    await firebaseUser.reload();
    firebaseUser = _auth.currentUser!;

    if (firebaseUser != null) {
      setState(() {
        user = firebaseUser;
        isLoggedIn = true;
      });
    }
  }

  logout() async {
    await _auth.signOut();

    final googleSignIn = GoogleSignIn();
    await googleSignIn.signOut();
  }

  @override
  void initState() {
    super.initState();
    checkAuthentification();
    getUser();
  }

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: <Widget>[
        SliverAppBar(
          title: Text("TIKI.VN"),
          actions: <Widget>[
            Stack(
              children: <Widget>[
                IconButton(
                  icon: Icon(Icons.shopping_cart),
                  onPressed: () {},
                ),
                Positioned(
                    top: 4.0,
                    right: 8.0,
                    child: Container(
                      width: 16.0,
                      height: 16.0,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.deepOrangeAccent,
                      ),
                      child: Text(
                        "3",
                        style: TextStyle(
                            fontSize: 12.0, fontWeight: FontWeight.bold),
                      ),
                    ))
              ],
            )
          ],
          expandedHeight: 200.0,
          // pinned: true,
          bottom: PreferredSize(
            preferredSize: const Size.fromHeight(120.0),
            child: Container(
                padding: const EdgeInsets.all(16.0),

                child: Row(
                  children: <Widget>[
                    Expanded(
                        child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "Chào mừng ${user?.displayName} đến với Tiki!",
                          style: TextStyle(
                            fontSize: 23.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.white
                          ),
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 8.0),
                          child: Text(
                            "${user?.email}",
                            style: TextStyle(
                              fontSize: 18.0,
                                color: Colors.white),),
                        ),
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(primary: Colors.red),
                          onPressed: logout,
                          child: Text("LOG OUT"),
                        )
                      ],
                    ))
                  ],
                )),
          ),
        ),
        SliverList(
            delegate: SliverChildListDelegate([
          Box(
            title: "Đơn hàng của tôi",
            action: TextButton(
              onPressed: () {},
              child: Text("Xem lịch sử"),
            ),
            child: Container(
              height: 100.0,
              child: GridView.count(
                crossAxisCount: 5,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Container(
                        child: Icon(
                          CommunityMaterialIcons.wallet_outline,
                          color: Colors.blue,
                        ),
                      ),
                      Expanded(
                          child: Container(
                        padding: EdgeInsets.only(top: 16.0),
                        child: Text(
                          "Chờ thanh toán",
                          textAlign: TextAlign.center,
                        ),
                      ))
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      Container(
                        child: Icon(
                          CommunityMaterialIcons.archive,
                          color: Colors.blue,
                        ),
                      ),
                      Expanded(
                          child: Container(
                        padding: EdgeInsets.only(top: 16.0),
                        child: Text(
                          "Đang xử lý",
                          textAlign: TextAlign.center,
                        ),
                      ))
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      Container(
                        child: Icon(
                          CommunityMaterialIcons.truck_check_outline,
                          color: Colors.blue,
                        ),
                      ),
                      Expanded(
                          child: Container(
                        padding: EdgeInsets.only(top: 16.0),
                        child: Text(
                          "Đang vận chuyển",
                          textAlign: TextAlign.center,
                        ),
                      ))
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      Container(
                        child: Icon(
                          CommunityMaterialIcons.book_check_outline,
                          color: Colors.blue,
                        ),
                      ),
                      Expanded(
                          child: Container(
                        padding: EdgeInsets.only(top: 16.0),
                        child: Text(
                          "Đã giao",
                          textAlign: TextAlign.center,
                        ),
                      ))
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      Container(
                        child: Icon(
                          Icons.restore,
                          color: Colors.blue,
                        ),
                      ),
                      Expanded(
                          child: Container(
                        padding: EdgeInsets.only(top: 16.0),
                        child: Text(
                          "Đổi trả",
                          textAlign: TextAlign.center,
                        ),
                      ))
                    ],
                  ),
                ],
                // scrollDirection: Axis.horizontal,
                childAspectRatio: 1.0,
              ),
            ),
          )
        ])),
        SliverToBoxAdapter(
          child: Container(
            color: Colors.white,
            margin: EdgeInsets.symmetric(vertical: 8.0),
            child: ListTile(
              leading: Icon(
                CommunityMaterialIcons.gift,
                size: 40.0,
                color: Colors.amberAccent,
              ),
              title: Text("Săn thưởng"),
              // subtitle: Text(
              //   "Các ưu đãi cho chủ thẻ ngân hàng",
              //   style: TextStyle(fontSize: 12.0),
              // ),
              trailing: IconButton(
                icon: Icon(
                  Icons.chevron_right,
                  size: 32.0,
                  color: Colors.blue,
                ),
                onPressed: () {},
              ),
            ),
          ),
        ),
        SliverToBoxAdapter(
          child: Container(
            color: Colors.white,
            margin: EdgeInsets.symmetric(vertical: 8.0),
            child: ListTile(
              leading: Icon(
                CommunityMaterialIcons.star_face,
                size: 40.0,
                color: Colors.blue,
              ),
              title: Text("Đánh giá sản phẩm"),
              trailing: IconButton(
                icon: Icon(
                  Icons.chevron_right,
                  size: 32.0,
                  color: Colors.blue,
                ),
                onPressed: () {},
              ),
            ),
          ),
        ),
        SliverToBoxAdapter(
          child: Container(
            color: Colors.white,
            margin: EdgeInsets.symmetric(vertical: 8.0),
            child: ListTile(
              leading: Icon(
                CommunityMaterialIcons.cart_check,
                size: 40.0,
                color: Colors.amberAccent,
              ),
              title: Text("Sản phẩm đã mua"),
              trailing: IconButton(
                icon: Icon(
                  Icons.chevron_right,
                  size: 32.0,
                  color: Colors.blue,
                ),
                onPressed: () {},
              ),
            ),
          ),
        ),
        SliverList(
            delegate: SliverChildListDelegate([
          BoxDetails(
            title: "Quan tâm",
            child: Container(
              height: 80.0,
              child: GridView.count(
                crossAxisCount: 4,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Container(
                        child: Icon(
                          CommunityMaterialIcons.eye_circle,
                          color: Colors.green,
                        ),
                      ),
                      Expanded(
                          child: Container(
                        padding: EdgeInsets.only(top: 16.0),
                        child: Text(
                          "Đã xem",
                          textAlign: TextAlign.center,
                        ),
                      ))
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      Container(
                        child: Icon(
                          CommunityMaterialIcons.heart_circle,
                          color: Colors.pinkAccent,
                        ),
                      ),
                      Expanded(
                          child: Container(
                        padding: EdgeInsets.only(top: 16.0),
                        child: Text(
                          "Yêu thích",
                          textAlign: TextAlign.center,
                        ),
                      ))
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      Container(
                        child: Icon(
                          CommunityMaterialIcons.briefcase_clock,
                          color: Colors.orange,
                        ),
                      ),
                      Expanded(
                          child: Container(
                        padding: EdgeInsets.only(top: 16.0),
                        child: Text(
                          "Mua sau",
                          textAlign: TextAlign.center,
                        ),
                      ))
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      Container(
                        child: Icon(
                          CommunityMaterialIcons.storefront,
                          color: Colors.blue,
                        ),
                      ),
                      Expanded(
                          child: Container(
                        padding: EdgeInsets.only(top: 16.0),
                        child: Text(
                          "Theo dõi",
                          textAlign: TextAlign.center,
                        ),
                      ))
                    ],
                  ),
                ],
                // scrollDirection: Axis.horizontal,
                childAspectRatio: 1.0,
              ),
            ),
          )
        ])),
        SliverToBoxAdapter(
          child: Container(
            color: Colors.white,
            margin: EdgeInsets.symmetric(vertical: 8.0),
            child: ListTile(
              leading: Icon(
                CommunityMaterialIcons.face_agent,
                size: 40.0,
                color: Colors.green,
              ),
              title: Text("Trung tâm trợ giúp"),
              // subtitle: Text(
              //   "Các ưu đãi cho chủ thẻ ngân hàng",
              //   style: TextStyle(fontSize: 12.0),
              // ),
              trailing: IconButton(
                icon: Icon(
                  Icons.chevron_right,
                  size: 32.0,
                  color: Colors.blue,
                ),
                onPressed: () {},
              ),
            ),
          ),
        ),
        SliverList(
            delegate: SliverChildListDelegate([
          BoxDetails(
            title: "Tiện ích khác",
            child: Container(
              height: 100.0,
              child: GridView.count(
                crossAxisCount: 4,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Container(
                        child: Icon(
                          CommunityMaterialIcons.book_open_page_variant_outline,
                          color: Colors.blue,
                        ),
                      ),
                      Expanded(
                          child: Container(
                        padding: EdgeInsets.only(top: 16.0),
                        child: Text(
                          "BookCare",
                          textAlign: TextAlign.center,
                        ),
                      ))
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      Container(
                        child: Icon(
                          CommunityMaterialIcons.headphones,
                          color: Colors.blue,
                        ),
                      ),
                      Expanded(
                          child: Container(
                        padding: EdgeInsets.only(top: 16.0),
                        child: Text(
                          "Hỗ trợ",
                          textAlign: TextAlign.center,
                        ),
                      ))
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      Container(
                        child: Icon(
                          Icons.location_on_outlined,
                          color: Colors.blue,
                        ),
                      ),
                      Expanded(
                          child: Container(
                        padding: EdgeInsets.only(top: 16.0),
                        child: Text(
                          "Số địa chỉ",
                          textAlign: TextAlign.center,
                        ),
                      ))
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      Container(
                        child: Icon(
                          CommunityMaterialIcons.credit_card_settings_outline,
                          color: Colors.blue,
                        ),
                      ),
                      Expanded(
                          child: Container(
                        padding: EdgeInsets.only(top: 16.0),
                        child: Text(
                          "Thông tin thanh toán",
                          textAlign: TextAlign.center,
                        ),
                      ))
                    ],
                  ),
                ],
                // scrollDirection: Axis.horizontal,
                childAspectRatio: 1.0,
              ),
            ),
          )
        ])),
        SliverToBoxAdapter(
          child: Container(
            color: Colors.white,
            margin: EdgeInsets.symmetric(vertical: 8.0),
            child: ListTile(
              leading: Icon(
                CommunityMaterialIcons.store,
                size: 40.0,
                color: Colors.blueAccent,
              ),
              title: Text("Bán hàng cùng Tiki"),
              // subtitle: Text(
              //   "Các ưu đãi cho chủ thẻ ngân hàng",
              //   style: TextStyle(fontSize: 12.0),
              // ),
              trailing: IconButton(
                icon: Icon(
                  Icons.chevron_right,
                  size: 32.0,
                  color: Colors.blue,
                ),
                onPressed: () {},
              ),
            ),
          ),
        ),
      ],
    );
  }
}
