import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'models/models.dart' as Models;

List<ImageProvider> home_banners = [
  AssetImage("images/banners/banner1.jpg"),
  AssetImage("images/banners/banner2.jpg"),
  AssetImage("images/banners/banner3.jpg"),
  AssetImage("images/banners/banner4.jpg"),
];

List<Models.Category> categories = [
  new Models.Category(name: "Balo", image: AssetImage("images/categories/balo.jpg")),
  new Models.Category(name: "Giày thể thao nam", image: AssetImage("images/categories/giay_the_thao_nam.jpg")),
  new Models.Category(name: "Sách tư duy - Kỹ năng sống", image: AssetImage("images/categories/sach_tu_duy.jpg")),
  new Models.Category(name: "Smartphones", image: AssetImage("images/categories/smartphone.jpg")),
  new Models.Category(name: "Truyện tranh, Manga, Comics", image: AssetImage("images/categories/comic_manga.jpg")),
  new Models.Category(name: "Bình giữ nhiệt", image: AssetImage("images/categories/binh_giu_nhiet.jpg")),
  new Models.Category(name: "Kem và sữa dưỡng da", image: AssetImage("images/categories/kem_duong_da.jpg")),
  new Models.Category(name: "Bông tắm, sữa tắm & xà phòng", image: AssetImage("images/categories/xa_phong.jpg")),
  new Models.Category(name: "Bàn ghế làm việc", image: AssetImage("images/categories/ban_ghe_lam_viec.jpg")),
  new Models.Category(name: "Xe máy", image: AssetImage("images/categories/xemay.jpg")),
  new Models.Category(name: "Phụ kiện nhà bếp", image: AssetImage("images/categories/phu_kien_nha_bep.jpg")),
  new Models.Category(name: "Tiểu thuyết", image: AssetImage("images/categories/tieuthuyet.jpg")),
  new Models.Category(name: "Kem chống nắng", image: AssetImage("images/categories/kem_chong_nang.jpg")),
  new Models.Category(name: "Bia nội địa", image: AssetImage("images/categories/bia_noi_dia.jpg")),
  new Models.Category(name: "Đầm dáng xòe", image: AssetImage("images/categories/dam_dang_xoe.jpg")),
];

List<Models.Product> products = [
  Models.Product(
    name: 'Balo nam nữ đi học Ohazo! Phát Sáng Cao Cấp, Balo Thời Trang Hót 2021 ( Tặng cáp sạc USB + Khóa chống Trộm)',
    image: NetworkImage(
      'https://salt.tikicdn.com/cache/200x200/ts/product/12/5d/4d/b7fc2e5e8fe854e75ee051adb1ee8292.jpg'
    ),
    salePrice: "169.000 ₫",
    regularPrice: "175.000 ₫",
  ),
  Models.Product(
    name: 'Balo Ulzzang Hàn Quốc Thời Trang HARAS HR284',
    image: NetworkImage(
        'https://salt.tikicdn.com/cache/200x200/ts/product/89/71/1d/845df27c2ce7ef3da08e907d1530e0b5.jpg'
    ),
    salePrice: "147.000 ₫",
    regularPrice: "250.000 ₫",
  ),
  Models.Product(
    name: 'Balo Ulzzang Hàn Quốc Thời Trang PRAZA - BLTK185 (Đen)',
    image: NetworkImage(
        'https://salt.tikicdn.com/cache/200x200/ts/product/8a/77/c6/6f99a926b98b15981be0068a0f666397.png'
    ),
    salePrice: "159.000 ₫",
    regularPrice: "249.000 ₫",
  ),
  Models.Product(
    name: "Giày Thể Thao Nam Biti's Hunter Street Vintage Blue DSMH04000XDL (Xanh Dương Lợt)",
    image: NetworkImage(
        'https://salt.tikicdn.com/cache/200x200/ts/product/26/71/21/a5f8f6cf9c2ca1dcac2b70c4c383c7ad.jpg'
    ),
    salePrice: "380.000 ₫",
    regularPrice: "633.000 ₫",
  ),
  Models.Product(
    name: "Giày Thể Thao Nam Biti’s Hunter X Z MIDNIGHT BLACK MID DSMH06301DEN (Đen)",
    image: NetworkImage(
        'https://salt.tikicdn.com/cache/200x200/ts/product/66/d5/37/217d1828d0da4a5015498b3d5c061697.jpg'
    ),
    salePrice: "1.207.000 ₫",
    regularPrice: "1.207.000 ₫",
  ),
  Models.Product(
    name: "Giày Thể Thao Cao Cấp Nam Biti's Hunter Layered Upper DSMH02800DEN (Đen)",
    image: NetworkImage(
        'https://salt.tikicdn.com/cache/200x200/ts/product/c9/b5/db/ec0116477e330664dfdb0e8e76811320.jpg'
    ),
    salePrice: "792.000 ₫",
    regularPrice: "932.000 ₫",
  ),
  Models.Product(
    name: "Giày bóng đá sân cỏ nhân tạo - Bản nâng cấp",
    image: NetworkImage(
        'https://salt.tikicdn.com/cache/200x200/ts/product/3a/36/70/c1ee6f9dea64d518ab321cad7a2b76d6.jpg'
    ),
    salePrice: "134.000 ₫",
    regularPrice: "225.000 ₫",
  ),
  Models.Product(
    name: 'Chú Thuật Hồi Chiến: Tập 1',
    image: NetworkImage(
        'https://salt.tikicdn.com/cache/200x200/ts/product/f6/31/92/989371cd53ac84a1baa221c7932b6b6e.png'
    ),
    salePrice: "27.900 ₫",
    regularPrice: "30.000 ₫",
  ),
  Models.Product(
    name: 'Thám tử lừng danh Conan Tập 99',
    image: NetworkImage(
        'https://salt.tikicdn.com/cache/200x200/ts/product/f0/bd/00/0c66fb626acd661f81799bde24dd9e47.jpg'
    ),
    salePrice: "19.000 ₫",
    regularPrice: "20.000 ₫",
  ),
  Models.Product(
    name: 'Thám Tử Kindaichi (Series Kỷ Niệm 20 Năm) - Boxset 5 Tập',
    image: NetworkImage(
        'https://salt.tikicdn.com/cache/200x200/ts/product/03/0d/95/174edb68dec9a3aead365c732759d5c5.jpg'
    ),
    salePrice: "156.000 ₫",
    regularPrice: "170.000 ₫",
  ),
  Models.Product(
    name: 'Điện Thoại iPhone 13 128GB  - Hàng Chính Hãng',
    image: NetworkImage(
        'https://salt.tikicdn.com/cache/200x200/ts/product/9e/44/ea/2b7ba151d4de1904beca5a66d383dad4.jpg'
    ),
    salePrice: "20.450.000 ₫",
    regularPrice: "20.450.000 ₫",
  ),
  Models.Product(
    name: 'Điện Thoại iPhone 13 Pro Max 128GB  - Hàng Chính Hãng',
    image: NetworkImage(
        'https://salt.tikicdn.com/cache/200x200/ts/product/dc/e8/5f/a5cfef7dd19655fc5b60ff214274a041.jpg'
    ),
    salePrice: "29.490.000 ₫",
    regularPrice: "29.490.000 ₫",
  ),
  Models.Product(
    name: 'Điện Thoại Samsung Galaxy Z Fold 3 (512GB) - Hàng Chính Hãng',
    image: NetworkImage(
      'https://salt.tikicdn.com/cache/200x200/ts/product/86/7c/6a/8b6fd25e1c01c3e9ce7bc89781eeb25f.jpg'
    ),
    salePrice: "42.490.000 ₫",
    regularPrice: "44.990.000 ₫",
  ),
  Models.Product(
    name: 'Điện Thoại Samsung Galaxy S22 Ultra 5G (12GB/512GB) - Hàng Chính Hãng',
    image: NetworkImage(
      'https://salt.tikicdn.com/cache/200x200/ts/product/fd/00/46/10b21d5f072a90197655a473d693295b.jpg'
    ),
    salePrice: "35.607.000 ₫",
    regularPrice: "35.607.000 ₫",
  ),
  Models.Product(
    name: 'Bàn Làm Việc Phong Cách Bắc Âu Thương Hiệu IGEA (Không Bao Gồm Ghế)',
    image: NetworkImage(
        'https://salt.tikicdn.com/cache/200x200/ts/product/ca/f2/50/699d7377206126d7041ee1626e6b5421.png'
    ),
    salePrice: "426.000 ₫",
    regularPrice: "426.000 ₫",
  ),
  Models.Product(
    name: 'Thùng Bia Tiger 24 Lon (330ml / Lon)',
    image: NetworkImage(
        'https://salt.tikicdn.com/cache/200x200/ts/product/6c/81/6f/0c95e4db25d2e119b132bfdaa6170246.png'
    ),
    salePrice: "375.000 ₫",
    regularPrice: "390.000 ₫",
  ),
  Models.Product(
    name: 'Thùng 24 lon cao Heineken Silver (mới) (330ml/lon)',
    image: NetworkImage(
        'https://salt.tikicdn.com/cache/200x200/ts/product/c0/ff/5c/ef52606a9c75cc6f23a6cd130468537a.png'
    ),
    salePrice: "470.000 ₫",
    regularPrice: "470.000 ₫",
  ),
  Models.Product(
    name: 'Kem Dưỡng Làm Dịu Kích Ứng Và Phục Hồi Da La Roche-Posay Cicaplast Baume B5 (40ml)',
    image: NetworkImage(
        'https://salt.tikicdn.com/cache/200x200/ts/product/de/07/51/41efbd69d0fd3554fc95b82971e75c96.jpg'
    ),
    salePrice: "349.000 ₫",
    regularPrice: "365.000 ₫",
  ),
  Models.Product(
    name: 'Gel Dưỡng Ẩm Nha Đam Innisfree Aloe Revital Soothing Gel 300ml - 131170177',
    image: NetworkImage(
        'https://salt.tikicdn.com/cache/200x200/media/catalog/producttmp/9f/c0/ed/992388a3d4778bf151fcafe78696e277.jpg'
    ),
    salePrice: "144.000 ₫",
    regularPrice: "160.000 ₫",
  ),
  Models.Product(
    name: 'Kem dưỡng làm mờ lỗ chân lông đá tro núi lửa innisfree Volcanic Pore Mattifying Cream 50ml - 131173333x',
    image: NetworkImage(
        'https://salt.tikicdn.com/cache/200x200/ts/product/37/4a/38/191e12e4a2d3c6b29ca1e2349a6195fc.jpg'
    ),
    salePrice: "468.000 ₫",
    regularPrice: "520.000 ₫",
  ),
  Models.Product(
    name: 'Cây Cam Ngọt Của Tôi',
    image: NetworkImage(
        'https://salt.tikicdn.com/cache/200x200/ts/product/5e/18/24/2a6154ba08df6ce6161c13f4303fa19e.jpg'
    ),
    salePrice: "77.100 ₫",
    regularPrice: "108.000 ₫",
  ),
  Models.Product(
    name: 'Kiêu Hãnh Và Định Kiến (Tái Bản)',
    image: NetworkImage(
        'https://salt.tikicdn.com/cache/200x200/ts/product/13/1a/c5/8de00e3c753d0d571b793caeab329d8d.jpg'
    ),
    salePrice: "89.300 ₫",
    regularPrice: "116.000 ₫",
  ),
  Models.Product(
    name: 'Những Giấc Mơ Ở Hiệu Sách Morisaki',
    image: NetworkImage(
        'https://salt.tikicdn.com/cache/200x200/media/catalog/product/1/x/1x2acr3d.u4972.d20170419.t115542.562440.jpg'
    ),
    salePrice: "46.200 ₫",
    regularPrice: "60.000 ₫",
  ),
];