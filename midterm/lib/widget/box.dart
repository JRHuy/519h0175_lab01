import 'package:flutter/material.dart';

class Box extends StatelessWidget {
  Box({required this.title, required this.action, required this.child});

  late final String title;
  late final Widget action;
  late final Widget child;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.all(16.0),
      // margin: EdgeInsets.symmetric(vertical: 8.0),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(title, style: TextStyle(
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold),
              ),
              action,
            ],
          ),
          child
        ],
      ),
    );
  }
}
