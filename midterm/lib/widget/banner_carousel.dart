import 'package:flutter/material.dart';

class BannerCarousel extends StatefulWidget {
  // const BannerCarousel({Key? key}) : super(key: key);

  BannerCarousel({required this.items, required double aspectRatio});

  List<ImageProvider> items;

  @override
  State<BannerCarousel> createState() => _BannerCarouselState();
}

class _BannerCarouselState extends State<BannerCarousel> {
  int currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    List<Widget> pages = widget.items
        .map((image) => Image(
              image: image,
              fit: BoxFit.cover,
            ))
        .toList();

    List<Widget> dots = List.generate(widget.items.length, (index) {
      return Container(
        height: 8.0,
        width: 8.0,
        margin: EdgeInsets.symmetric(horizontal: 4.0),
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: index == currentIndex ? Colors.white : Colors.grey,
        ),
      );
    });
    
    return SliverToBoxAdapter(
      // child: Container(
      //   height: 135.0,
      //   child: PageView(children: pages,),
      // )
      child: AspectRatio(
        aspectRatio: 3.0,
        child: Stack(
          children: <Widget>[
            PageView(
              children: pages,
              onPageChanged: (nextIndex) {
                setState(() {
                  currentIndex = nextIndex;
                });
              },
            ),
            Positioned(
              bottom: 24.0,
              left: 0.0,
              right: 0.0,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: dots,
              )
            )
          ],
        ),
      ),
    );
  }
}
