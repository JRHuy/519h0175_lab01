import 'package:flutter/widgets.dart';

class Category {
  String name;
  ImageProvider image;

  Category({required this.name, required this.image});
}

class Product {
  String name;
  ImageProvider image;
  String salePrice;
  String regularPrice;
  // double rating;
  // int reviewCount;
  // List badges;

  Product({
    required this.name,
    required this.image,
    required this.salePrice,
    required this.regularPrice,
    // required this.rating,
    // required this.reviewCount,
    // required this.badges,
  });
}
