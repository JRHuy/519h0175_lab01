import 'package:flutter/material.dart';
import 'package:midterm/views/home_screen.dart';

class MyTiki extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Tiki",
      debugShowCheckedModeBanner: false,
      home: HomeScreen(),
    );
  }
}