import 'dart:async';
import 'dart:io';
import 'package:http/http.dart' as http;

class SakaiService {
  String sakaiUrl;

  String? cookie;
  String? token;

  SakaiService({required this.sakaiUrl});

  Future<http.Response> authenticate(String username, String password) async {
    var map = new Map<String, dynamic>();
    map['_username'] = username;
    map['_password'] = password;

    final response = await http.post(
      Uri.parse('$sakaiUrl/direct/session'),
      body: map,
    );

    if (response.statusCode == 200 || response.statusCode == 201) {
      cookie = parseCookieJSessionID(response);
      token = response.body;
    }
    return response;
  }

  String? parseCookieJSessionID(http.Response response) {
    var jsessionId = response.headers['set-cookie'];

    RegExp exp = RegExp(r"(JSESSIONID=[\w-.]+);");
    var matcher = exp.firstMatch(jsessionId as String);

    return matcher?.group(1);
  }

  Future<http.Response> checkSession() async {
    final response = await http.get(
      Uri.parse('$sakaiUrl/direct/session/current.json'),
    );

    return response;
  }


  Future<http.Response> getSites() async {
    return http.get(
      Uri.parse('$sakaiUrl/direct/site.json'),
      headers: {
        HttpHeaders.authorizationHeader: 'Bear $token',
        HttpHeaders.cookieHeader: '$cookie'
      },
    );
  }

}